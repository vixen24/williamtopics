---
layout: page
title: About
permalink: /about
---
<br>
<center>
    <figure class="about-headshot" >
        <img src="{{ "/assets/images/headshot.png" | relative_url }}" alt="WilliamTopics">
    </figure>
    <p class="about-name"><a href="https://www.linkedin.com/in/idakwowilliam" target="_blank">William Idakwo</a></p>
</center> 

> Highly technical cybersecurity professional with half a decade of experience, specializing in cloud security, application security, DevSecOps, Governance, Risk and Compliance (GRC) and vulnerability management. My background cuts across multiple industries - from federal to financial to non-profit and for-profit organizations.

Career highlights include:

* Led multiple cybersecurity engagements spanning over fifteen (15) African countries - everything from architecture to engineering to analysis & automation. This involves overseeing the creation, implementation and continual improvement of security programs. Some of these program include:
    * IT Infrastructure/Application Modernization
    * Information Risk and Security
    * Application Security
    * Vulnerability Management
    * Business Continuity and Disaster Recovery
    * Service Management <br><br>

* Review security policies and practices, perform risk assessment, plan risk remediation, and provide actionable feedback to senior management.

* Leverage scripting (Powershell, Python and Shell) to solve security automation challenges, advance security maturity and achieve principles of DevSecOps.

* Implement robust controls to protect sensitive data, integrated security controls in all phases of CI/CD pipelines, carry out security assessment and audit to ensure compliance with security policies and regulatory requirements.

For more about me, check out my LinkedIn page. 

#### Education
* B.Sc in Electrical and Electronics Engineerning, Eastern Mediterranean University (2017)

#### Certifications
CompTIA Sec+ | AWS Certified Solution Architect | AWS Certified Developer | Azure Fundamentals

#### Skills/Tools
* Privacy & Risk Management + Assessment : ISO 27001, ISO 22301, ISO 20000, NIST, GDPR, PCI
* Cloud Security Audit + Assessment
* Scripting, Automation & DevSecOps : Python, Shell, Powershell 
* Cloud Security + Architecture : AWS, CloudFormation, Terraform
* Vulnerability Management + Threat Intelligence : Nessus, Qualys, Spiderfoot, Prisma Cloud
* Third-Party / Supply-Chain Security