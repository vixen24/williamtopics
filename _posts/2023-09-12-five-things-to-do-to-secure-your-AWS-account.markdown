---
layout: post
title:  "Five things to do to secure your AWS account"
date:   2023-09-12 20:15:00 +0100
category: blog
coverimage: /assets/images/aws_security_best_practices.png
image:
 path: /assets/images/aws_security_best_practices.png
 width: 1200
 height: 630
Tag: AWS security, AWS best practices
readtime: "15mins read"
excerpt: AWS strongly recommends that you do not use the root user for your everyday tasks...
---
Have you opened an AWS account recently or do you have an exisiting AWS account, here are five (5) things that you can do to secure you AWS account. 

1. Restrict the use of the root user
2. Enable password complexity requirement 
3. Enable Multifactor Authentication (MFA)
4. Enable Cost Explorer
5. Configure AWS Budgets to track your usage


**1\. Restrict the use of the root user**
> **Action**    
> Create an [Administrator account]()

The root user is created when you create an AWS account, and this user has full ownership privileges and permissions over the account that cannot be changed. You should protect your root user credentials like you would your credit card numbers or any other sensitive secret and use them for only the tasks that require them. 
* Your root user credentials is the master key, anybody with access to it has unfettered access to your AWS account, even AWS support might not be able to help you to regain control of your account if a rogue user hijacks it. 
* The root user should be used for tasks that can be performed by only the root user such as *closing your AWS account*. Daily task should be managed using an administrator account.
* Don't create access keys for the root user. Instead, use the root user to [create an administrator account]() for daily task. 

**2\. Enable password complexity requirement**  
> **Action**    
> Enable [password complexity requirement]()

* Many AWS accounts have been compromised in the wild as a result of the use of weak passwords. Enforcing password complexity requirements ensures that all users are forced to use passwords that meets the minimum requirements.  
* Never share your AWS account root user password or access keys with anyone
* Use a strong password to help protect access to the AWS Management Console.

**3\. Enable Multifactor Authentication (MFA)**  
> **Action**    
> Enable [Multifactor Authentication (MFA)]()

MFA adds extra level of security because it requires users to provide unique authentication from an AWS supported MFA mechanism in addition to their regular sign-in credentials when they access AWS websites or services. To secure your AWS account and force all users to set MFA, follow the action above. 

**4\. Enable Cost Explorer**  
> **Action**    
> Enable [Cost Explorer]()   

AWS Cost Explorer is a tool that enables you to view and analyze your costs and usage. You can view the breakdown of your cost and the services that generated the cost, and view your usage pattern over a period of time.


**5\. Configure AWS Budgets to track your usage**  
> **Action**    
> Turn on [AWS Free Tier Usage alerts]()   
> Configure [AWS Budgets to track your usage]() 

It is very common for developers to provision resources (such as an EC2 instance) and forget to delete it when they are done with testing and development. Cost tracking is important because it helps you to monitor resource utlilzation and reduce unnecessary expense. 

First things first, track your AWS Free Tier Usage. To do this, turn on Free Tier usage alerts in Billing preferences to automatically notify you over email when you exceed 85% of the Free Tier limit for each service. See Action above. 

AWS Budgets can be used to track and take action on your AWS costs and usage. In this case, we will set a monthly cost budget with a fixed target amount to track all costs associated with your account and send an email notification when the set budget is exceeded. See Action above to configure AWS Budgets 

<br>

References: [AWS Account Management](https://docs.aws.amazon.com/accounts/latest/reference/best-practices-root-user.html), 
[AWS IAM](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_credentials_passwords_account-policy.html), 
[AWS Prescriptive Guidance](https://docs.aws.amazon.com/prescriptive-guidance/latest/aws-startup-security-baseline/controls-acct.html), 
[AWS Billing](https://docs.aws.amazon.com/awsaccountbilling/latest/aboutv2/tracking-free-tier-usage.html), 
[AWS Cost Management](https://docs.aws.amazon.com/cost-management/latest/userguide/ce-enable.html)
