---
layout: post
title:  "Part I: Deploy Static Web App from GitHub to Azure Static Web App"
date:   2024-08-12 08:00:00 +0100
category: blog
coverimage: /assets/images/github-workflow.webp
image:
 path: /assets/images/github-workflow.webp
 width: 1200
 height: 630
tag: azure
readtime: "1hr read"
excerpt: Azure App Service is an HTTP-based service for hosting web applications, REST APIs, and mobile back ends...
---

We would go through a few things

  - Azure App Service
  - App Service Plan
  - Deploy static web app from GitHub to Azure Static Web App

If you are not interested in the literature, just skip to the deploy section. 
<br>
<br>

## Azure App Service

Azure App Service is an **HTTP-based service** for hosting web applications, REST APIs, and mobile back ends. It is a Platform as a Service (PaaS) offering from Microsoft.

When you use App Service, you don’t have to worry about, managing the network, infrastructure, installing operating system updates, critical patches, runtime or middleware components. You just focus on building and deploying applications and services.

App Service adds security, load balancing, auto scaling, and automated management features of Microsoft Azure to your application. It also has DevOps features including continuous deployment via Azure DevOps, GitHub, Docker Hub, and other sources, package management, staging environments, custom domains, and TLS/SSL certificates.

Azure App Service includes four application development and hosting environment.

1.	Web App	    
2.	Logic Apps
3.	Mobile App	
4.	API Apps

The App Service has six billing plans.    
1.	Free	
2.	Shared	
3.	Basic	
4.  Standard
5.  Premium
6.  Isolated

**Free, shared (preview) and Basic plans** provide different options to test your apps within your budget. **Standard and Premium plans** are for production workloads and run on dedicated Virtual Machine instances.

App Service automatically patches and maintains the OS and language frameworks for you. You can spend time writing great apps and let Azure worry about the platform.

App Service supports different languages and also run PowerShell and other scripts or executables as background services.

You can dockerize your app and host a custom Windows or Linux container in App Service. Run multi-container apps with Docker Compose. Migrate your docker skills directly to App Service.

For most scenarios, App Service is the best choice. For microservice architecture, consider Azure Spring Apps or Service Fabric. If you need more control over the VMs on which your code runs, consider Azure Virtual Machines.

**App Service Scaling**
Vertical scaling describes adding additional resources to a system so that it meets demand. These resources include Compute, power, memory or CPU.

Horizontal scaling refers to adding additional nodes or machines to your infrastructure to cope with new demands. This usually means increasing or decreasing the Instance Count. 

We can set different scale sets in Auto Scale options, schedule a scale set based on date and Time, (for example, during peak sale period, holidays etc.., can also repeat specific dates), and scale based on Metric and Scale based on Instance Count.

You can define the startup file of your App service application in General Settings->Configuration->Default Documents.

Each application will have only Single in-bound Aadress regardless of your Scale-out and multiple out-bound address.
Outbound Address is used to connect to Azure SQL, Storage, other Service etc.,
<br>
<br>

## App Service Plan

In App Service, an app (Web Apps, API Apps, or Mobile Apps) always runs in an App Service plan. An App Service plan defines a set of compute resources for a web app to run. One or more apps can be configured to run on the same computing resources (or in the same App Service plan). In addition, Azure Functions also has the option of running in an App Service plan.

Your App Service plan can be scaled up and down at any time. It is as simple as changing the pricing tier of the plan. If your app is in the same App Service plan with other apps, you may want to improve the app’s performance by isolating the compute resources. You can do this by moving the app into a separate App Service plan.

You can potentially save money by putting multiple apps into one App Service plan. However, since apps in the same App Service plan all share the same compute resources you need to understand the capacity of the existing App Service plan and the expected capacity for the new app.

Isolate your app into a new App Service plan when:

1.	The app is resource intensive.
2.	You want to scale the app independently from the other apps in the existing plan.
3.	The app needs resource in a different geographical region.

This way you can allocate a new set of resources for your app and gain greater control of your apps.

Each App Service plan defines:
•	Region (West US, East US, etc.)
•	Number of VM instances
•	Size of VM instances (Small, Medium, Large)
•	Pricing tier (Free, Shared, Basic, Standard, Premium, PremiumV2, PremiumV3, Isolated)

**Shared compute:** Both Free and Shared share the resource pools of your apps with the apps of other customers. These tiers allocate CPU quotas to each app that runs on the shared resources, and the resources can’t scale out.

**Dedicated compute:** The Basic, Standard, Premium, PremiumV2, and PremiumV3 tiers run apps on dedicated Azure VMs. Only apps in the same App Service plan share the same compute resources. The higher the tier, the more VM instances are available to you for scale-out.

**Isolated:** This tier runs dedicated Azure VMs on dedicated Azure Virtual Networks. It provides network isolation on top of compute isolation to your apps. It provides the maximum scale-out capabilities.

**Consumption:** This tier is only available to function apps. It scales the functions dynamically depending on workload.
An App Service plan can be deployed as a zone redundant service in the regions that support it. This is a deployment time only decision. You can’t make an App Service plan zone redundant after it has been deployed.


Sources: https://learn.microsoft.com/en-us/azure/app-service/overview-hosting-plans, https://learn.microsoft.com/en-us/azure/app-service/overview-hosting-plans
<br>
<br>

## Deploy static web app from GitHub to Azure Static Web App

### Prerequisite

- Azure account
  - If you don’t have an existing account, click the link to create a free Azure account before moving to the steps.
- GitHub account
  - If you don’t have a GitHub account, follow the steps in the link to create an account before moving forward.

In this section, you will be deploying a simple index.html file from your GitHub repository. 
The file is located in the csn-landing-site folder. 

**Steps:**
1.	Ensure you are signed into your GitHub account. 
2.	View the source code repository, go to [https://github.com/vixen24/csn-training-resources](https://github.com/vixen24/csn-training-resources)
3.	Fork the repository to make a copy in your GitHub account
    - Click on the `Fork` button at the top righthand corner of the repository and then, click `Create fork`.   
    <p></p>
    <center>
      <figure>
        <img src="/assets/images/github_fork.webp" alt="github repo">
        <figcaption>GitHub Repo showing fork button</figcaption>
      </figure>
    </center>

4.	Confirm your azure account is up and running, go to [portal.azure.com](portal.azure.com). 
5.	From the global search bar at the top, type Static Web Apps and select `Static Web Apps` from the dropdown menu. 
6.	Click `Create static web app`, located at the center of the webpage.  
7.	Create the Static Web App using the following parameters (See image below)
    - Subscription: _leave the default Azure subscription on your account_
    - Resource Group: click `Create new` and enter, web-app-rg. Then, click `Ok`
    - Name: static-web-app
    - Plan type: Free: For hobby or personal projects
    - Source: GitHub
    - GitHub account: _If your GitHub account is already linked to Azure App Service, your GitHub account name will be displayed. However, if it is not linked, click and follow the prompt to login._
    - Organization: click the dropdown and select your GitHub account name
    - Repository: csn-training-resources
    - Branch: main
    - Build Presets: HTML
    - App location: csn-landing-site
    - Api location: _leave blank_
    - Output location: csn-landing-site
8.	Click `Review + create`

    <center>
      <figure>
        <img src="/assets/images/static-web-app-summary.webp" alt="web app summary">
        <figcaption>Static Web App summary</figcaption>
      </figure>
    </center>

9.	Click `Create`
10.	Wait a few minutes until the Notification shows, Deployment succeeded
11.	Click `Go to Resource`, to open the static-web-app overview
12.	Click `URL`, to open the static web app in a new tab

    <center>
      <figure>
        <img src="/assets/images/static-web-app-deployed.webp" alt="web deployed">
        <figcaption>Deployed successfully</figcaption>
      </figure>
    </center>

<br>
> Lastly, deprovision resources you created to prevent unexpected billing at the end of the month.    
From step 11, click `delete button`, located at the top bar. Click `Yes`, to finally delete the static web app resource.

<br>
<br>

See [Part II: Deploy NodeJS app from GitHub to Azure Web App using GitHub actions]({% post_url 2024-08-12-deploy-nodejs-app-from-github-to-azure-web-app %})