---
layout: note
title:  "International BSOD day"
date:   2024-07-21 03:47:00 +0100
category: notes
readtime: "5mins read"
excerpt_separator: <!--more-->
---
Friday 20th, 2024 was supposed to be like every other Friday, after a stressful week we tgif. At least, this is what we all thought except Crowdstrike had other plan for us. Government offices were shutdown, Int'l and local airports, banks, schools, offices you name it, the BSOD had taken over windows PC thanks to a bad driver file from crowdstrike. As security professionals, is there anything we can do to prevent a similar situation in the future? I highly doubt that as we do not have kernel-level access to make changes to the OS, we can only pray and hope that the Crowdstrikes' of this world do the right thing. I look forward to see what controls Microsoft implements to prevent a reoccurrence.
 <!--more-->
