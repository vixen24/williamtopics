---
layout: post
title:  "Part II: Deploy NodeJS app from GitHub to Azure Web App using GitHub actions"
date:   2024-08-12 10:00:00 +0100
category: blog
coverimage: /assets/images/github-workflow.webp
image:
 path: /assets/images/github-workflow.webp
 width: 1200
 height: 630
tag: azure, cloud
readtime: "1hr read"
excerpt: In this section, we will focus on building and deploying our code to Azure web app using GitHub actions...
---

## Prerequisite

- Azure account
  - If you don’t have an existing account, click the link to create a free Azure account before moving to the steps.
- GitHub account
  - If you don’t have a GitHub account, follow the steps in the link to create an account before moving forward.

In this section, we will focus on building and deploying our code to Azure web app using GitHub actions. 

**Steps:**
1.	Login to [portal.azure.com](portal.azure.com), go to the global search bar at the top of the webpage, type App Services, then select `App Services` from the dropdown menu.
2.	Click `Create` and select `Web App` from the dropdown menu.
3.	Create the Web App using the following parameters, 
    - Subscription: _leave the default Azure subscription on your account_
    - Resource Group: web-app-rg
    - Name: react-nodejs-app
    - Publish: code
    - Runtime stack: NODE 18 LTS
    - Operating System: Linux
    - Region: East US
    - Linux Plan: Click `Create new`, enter web-app-sp. 
      > Note: This action automatically creates a new App Service Plan with Free F1 pricing plan. To view this, type App Service Plan in the global search bar and select it. 
    - Pricing plan: Free F1 (Shared Infrastructure)
4.	Click `Next`
    - Basic authentication: Enable
      > Note: This will allow permission for GitHub actions workflow to deploy code to Azure Web App.
5.	Click `Review + create`
6.	Review the summary. Then click, `Create`
7.	Wait a few minutes until the Notification shows, Deployment succeeded
8.	Click `Go to Resource`, to open the Web App overview
9.	Click `Download publish profile`. Save download for later. 
10.	View the source code repository, go to [https://github.com/vixen24/csn-training-web-app](https://github.com/vixen24/csn-training-web-app)
11.	Fork the repository to make a copy in your Github account
    - Click on the `Fork` button at the top right corner of the repository and then, click `Create fork`. 
12.	Click `Settings` on the forked repository. 
13.	Click `Secrets and variables`, located on the bottom left pane
14.	Click `Actions` on the dropdown menu
15.	Click `New repository secret`
16.	Enter the following parameters to create a secret variable.
    - Name: AZUREAPPSERVICE_PUBLISHPROFILE_9619
    - Secret: Refer to step 9, copy the content of the download file and paste it here. 
17.	Click `Add secret`
 
    <center>
      <figure>
        <img src="/assets/images/github_secret.webp" alt="Secrets and variables">
        <figcaption>Secrets and variables</figcaption>
      </figure>
    </center>

    The secret variable created will be used in the GitHub action workflow file located at .github/workflows. This ensures that our Web app publish profile is confidential. 

    To trigger a workflow run (aka CICD pipeline), we need to make a commit and push it to our main branch. To do this, we would edit the README.md file and commit the changes. See next steps

18.	Click `Code`, located at the top bar
19.	Click `README.md`
20.	Click the `Edit` button located the top right corner

    <center>
      <figure>
        <img src="/assets/images/github_edit_readme.webp" alt="README.md edit button">
        <figcaption>README.md edit button</figcaption>
      </figure>
    </center>

21.	Change Node.js >= v12 to Node.js = v18
22.	Click `commit changes`, located at the top right corner
23.	Click `commit changes` on the message box
24.	Click `Actions` located on the top bar, to view workflow run
25.	Wait a few minutes until workflow run has succeeded
    - The workflow run will build the application, and then deploy app to Azure Web App service. 
26.	Go to the Web App you created in step 5 above and click the `Default domain` to view the website.

    <center>
      <figure>
        <img src="/assets/images/react-nodejs-app-deployed.webp" alt="Web app deployed">
        <figcaption>Web app deployed</figcaption>
      </figure>
    </center>


<br>
> Lastly, deprovision resources you created to prevent unexpected billing at the end of the month. In the steps below we will delete web-app-rg resource group which contains all the resources we created. 
1.	From the global search bar at the top, type Resource groups, select `Resource groups` in the dropdown. 
2.	Click `web-app-rg` in the list of resource groups
3.	Click `Delete resource group` in the top bar
4.	Enter the name of the resource group and click `Delete`


<br>
<br>
<br>