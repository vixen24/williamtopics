---
layout: note
title:  "AWS Certifications"
date:   2024-06-01 17:00:00 +0100
category: notes
readtime: "5mins read"
excerpt_separator: <!--more-->
---
I've always been fascinated by computers, networks, the internet, and the web. However, cloud technology truly revolutionized the world of technology. While it's easy to understand that the cloud is essentially a collection of servers at a physical location, rented out for various needs, the question of "how is it feasible?" intrigued me.<!--more-->

Fast-forward to 2022, I embarked on a deep dive into Azure services, deploying Node.js applications on Azure Virtual Machines using Azure Pipelines, and even running Docker applications on the platform. This journey culminated in achieving the Azure Fundamentals certification.

A few months later, I was drawn to AWS, primarily due to its generous free-tier offering. Soon, I was deploying WordPress applications and building virtual infrastructure on AWS. To date, I have earned three AWS certifications namely AWS Certified Developer, AWS Solution Architect Associate, and AWS Certified Security Specialist certifications.

Currently, I'm preparing for the AWS Solution Architect Professional certification. My hands-on experience has made exam preparation relatively smooth. I typically complete Jason Stephane Maarek's AWS courses on Udemy and sit for the exam within a month or two.