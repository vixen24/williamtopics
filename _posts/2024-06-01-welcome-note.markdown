---
layout: note
title:  "Welcome Note"
date:   2024-06-01 17:00:00 +0100
category: notes
readtime: "5mins read"
excerpt_separator: <!--more-->
---
Today was just another Saturday that I was off from work, so I decided to add a new feature to my website. A little back story would help: I launched my website sometime in Sept 2023. I was very excited, because this would be my first time working on a web project from start to finish all by myself. However, it wasn't all easy peasy. A year prior to September 2023, I knew that I wanted a personal website but I didn't know what technologies to use, what cost might be involved, and basically how to go about it.<!--more-->
So as usual over the next few months I researched and delved into different technologies including react, wordpress, ejs, handlebars, Jekyll, Nodejs, Ruby-on-Rails (RoR) and many more. For each of these technologies I would create a POC and try to deploy locally and then on AWS/Azure. During these trails, I learned a lot about web development, web technologies and use cases, for example why use RoR over React and Nodejs combo? Well, the answer lies in the fact that though RoR is strongly opinionated it allows you to focus on developing an application without the hassle of packages and connectivity between components. On the other hand, a react and Nodejs combo is very scalable and allows for a more modern design philosophy and integrations with third-party components. In the end Jekyll won the battle, it is a perfect blend of what I needed, a Static Site Generator (SSG), lightweight, easily customizable, has an amazing community of supporters and indie developers, easy to setup with little to no maintenance. Oh, and it does not require a database. 

Enough of the rants, the new feature I talked about earlier is what you are currently engaging with. A note feature that allows for short tweet-like notes that is not in the long-post format. Even though the idea came in the morning, I was able to design, validate and push to production same day. Yeaah! anyways it feels good. 

The note feature was inspired by a fellow indie developer, shout out to Mike <a href="https://shellsharks.com" target="_blank">@shellsharks</a>

Welcome to my Notes!