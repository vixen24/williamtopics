---
layout: post
title:  "The Pyramid of Pain"
comments: true
date:   2024-08-26 14:00:00 +0100
category: blog
coverimage: /assets/images/pyramid-of-pain.webp
image:
 path: /assets/images/pyramid-of-pain.webp
 width: 1200
 height: 630
tag: security operation
readtime: "30mins read"
excerpt: The goal of detecting indicators (IOCs and IOAs) is to identify and mitigate potential threats. If you respond quickly enough, you will deny the adversary the use of those indicators in attacking you.
---

Lately, I have been reading a lot about frameworks, methodology and models for effective usecase management and threat hunting in the SOC. While reading, I came across an old blog post from [David J Bianco](https://detect-respond.blogspot.com/2013/03/the-pyramid-of-pain.html) talking about the Pyramid of Pain. In this post, I will shed more light on the Pyramid of Pain.

The goal of detecting indicators (IOCs and IOAs) is to identify compromised systems, detect ongoing attacks, investigate incidents and/or prevent future attacks. If you respond quickly enough to an ongoing attack, you might deny the adversary the use of those indicators in attacking you. Not all indicators are created equal, some of them are more valuable than others. Let's look at these indicators...

<br>

## Types of Indicators

1. **Hash Values:** SHA256, SHA1, MD5 or other similar hash algorithm that correspond to specific suspicious or malicious files.  Often used to provide unique references to specific samples of malware or to files involved in a security incident.

2. **IP Addresses:**  An IP address or an IP subnet

3. **Domain Names:** This could be either a domain name (e.g., "evil.net"), a sub-domain or a sub-sub-domain (e.g., "this.is.sooooo.evil.net")

4. **Network Artifacts:** Observables caused by adversary activities on your network. Technically speaking, every byte that flows over your network as a result of the adversary's interaction could be an artifact. In practice, this means pieces of activity that might tend to distinguish malicious activities from legitimate ones.  Typical examples might be URI patterns, C2 information embedded in network protocols, distinctive HTTP User-Agent or SMTP Mailer values, etc.

5. **Host Artifacts:** Observables caused by adversary activities on one or more of your hosts.  Again, we focus on things that would tend to distinguish malicious activities from legitimate ones.  They could be registry keys or values known to be created by specific pieces of malware, files or directories dropped in certain places or using certain names, names or descriptions or malicious services or almost anything else that's distinctive.

6. **Tools:** Software used by the adversary to accomplish their mission.  Mostly this will be things they bring with them, rather than software or commands that may already be installed on the computer.  This would include utilities designed to create malicious documents for spearphishing, backdoors used to establish C2 or password crackers or other host-based utilities they may want to use post-compromise.

7. **Tactics, Techniques and Procedures (TTPs):** How the adversary goes about accomplishing their mission, from reconnaissance all the way through data exfiltration and at every step in between.  "Spearphishing" is a common TTP for establishing a presence in the network.  Dumping cached authentication credentials and reusing them in Pass-the-Hash attacks, would be a TTP.  Notice we're not talking about specific tools here, as there are any number of ways of weaponizing a PDF or implementing Pass-the-Hash.

<br>
If you are like me you probably thought "The Pyramid of Pain" refers to the difficulty of detecting and triaging these indicators. However, that's not the case here. This diagram shows the relationship between the different indicators you might use to detect an adversary's activities and how much pain it will cause them when you are able to deny those indicators to them.

<br>

## The Pyramid Explained

The bottom of the pyramid is colored green, while the pinnacle of the pyramid is red.  Both the area and the colors are very important in understanding the value of the indicator. 

**Hash values**   
Hash algorithms compute a message digest that is unique to the input. In other words, if the content of two similar files defer by a single bit, the resultant hash values of the two files will be completely different. 

Hash indicators are the most accurate type of indicator you could hope for.  The odds of two different files having the same hash values are so low, you can almost discount this possibility altogether. However, any change to a file, even an inconsequential one like flipping a bit in an unused resource or adding a null to the end, results in a completely different and unrelated hash value.  It is so easy for hash values to change, and there are so many of them around, that in many cases it may not even be worth tracking them.  

**IP Addresses**    
IP addresses is a fundamental indicator, as most computers are part of a network whether its the internet or a LAN, both requiring IP addresses. It's at the widest part of the pyramid because there are just so many of them. Any reasonably advanced adversary can change IP addresses at will, with very little effort. In some cases, if they are using a anonymous proxy service like Tor, they may change IPs frequently and never even notice or care. That's why IP Addesses are green in the pyramid. If you deny the adversary the use of one of their IPs, they can usually recover without even breaking stride.

**Domain Names**    
One step higher on the pyramid, we have Domain Names (still green, but lighter). These are slightly more difficult to change, because in order to work, they must be registered, paid for (even if with stolen funds), and hosted somewhere. That being said, there are a large number of DNS providers in the wild with lax registration process, so in practice it's not difficult to change domains. New domains may take anywhere up to a fews hours to a few days to propagate the Internet. So, these are slightly harder to change than just IP addresses.

**Network & Host Artifacts**    
Right in the middle of the pyramid and starting to get into the yellow zone, we have the Network and Host Artifacts. This is the level, at last, where you start to have some negative impact on the adversary. When you can detect and respond to indicators at this level, you cause the attacker to go back to their lab and reconfigure and/or recompile their tools. A great example would be when you find that the attacker's HTTP recon tool uses a distinctive User-Agent string when searching your web content (off by one space or semicolon, for example. Or maybe they just put their name. Don't laugh. This happens!). If you block any requests which present this User-Agent, you force them to go back and spend some time figuring out how you detected their recon tool, and fixing it. Sure, the fix may be trivial, but at least they had to expend some effort to identify and overcome the obstacle you threw in front of them. 

**Tools**   
At this level, we are taking away the adversary's ability to use one or more specific arrows in their quiver.  Most likely this happens because we just got so good at detecting artifacts of their tool in so many different ways that they gave up and have to either find or create a new tool for the same purpose.  This is a big win for you, because they have to invest time in research (find an existing tool that has the same capabilities), development (create a new tool if they are able) and training (figure out how to use the tool and become proficient with it).  You just cost them some real time, especially if you are able to do this across several of their tools.

Some examples of tool indicators might include AV or Yara signatures, if they are able to find variations of the same files even with moderate changes.  Network aware tools with a distinctive communication protocol may also fit in this level, where changing the protocol would require substantial rewrites to the original tool.  Also, as discussed above, fuzzy hashes would probably fall into this level.

**Tactics, Techniques & Procedures**    
Finally, at the apex are the TTPs.  When you detect and respond at this level, you are operating directly on adversary behaviors, not against their tools.  For example, you are detecting Pass-the-Hash attacks themselves (perhaps by inspecting Windows logs) rather than the tools they use to carry out those attacks.  From a pure effectiveness standpoint, this level is your ideal.  If you are able to respond to adversary TTPs quickly enough, you force them to do the most time-consuming thing possible: learn new attack patterns.  

<br>

## Conclusion
Whenever you receive new intel on an adversary, review it carefully against the Pyramid of Pain.  For every indicator, ask yourself "Is there anything here I can use to detect the adversary's activity, and where does this fall on the pyramid?"  Sure, take all those domains names and IP addresses and make use of them if you can, but keep in mind that the amount of pain you cause an adversary depends on the types of indicators you are able to make use of. 