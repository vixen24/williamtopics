---
layout: page
title: About
permalink: /about
---
<br>

<h1 style="font-family: CaskaydiaCoveNerdFontMono;">Fancy About.</h1>

I love everything security, occassionally I enjoy building and breaking things &#128540;
<br>

William is a cybersecurity professional focused on helping organizations to better protect themselves against events that affects the confidentiality, integrity or availability of their systems or data.  He's had the opportunity to work across multiple security domains including security operations, security and risk management, and security in software development. 

He continuously challenges himself to learn new skills and technologies. From building his personal website to automating repetitive tasks using PowerShell to building infrastructure in the cloud.
<br>
<br>

## Education
* B.Sc in Electrical and Electronics Engineerning, Eastern Mediterranean University 
<br>
<br>

## Certifications
CompTIA Sec+ | AWS Certified Solution Architect | AWS Certified Developer | Azure Fundamentals
<br>
<br>

## Skills & Competencies
* GRC: ISO 27001, ISO 22301, ISO 20000, NIST, GDPR, PCI
* SOC, Threat Intelligence and Incident response
* Security Audit + Assessment
* Scripting, Automation & DevSecOps
* Cloud Security + Architecture 
<br>
<br>

For more about me, check out my [Linkedin.](https://www.linkedin.com/in/idakwowilliam){:target="_blank"}        
`Email:` idawills dot iw at gmail dot com     
`Address:` *redacted*

To send a confidential message,         
`PGP public key:`

{% include code-header.html %}
{% highlight C %}
-----BEGIN PGP PUBLIC KEY BLOCK-----

mQENBGacKeIBCACoChm4huD/JTXJrWU40gVIRF6y+afHj6uBFjKpQVdg6jQXIbZy
PcaVpsEf3J+VgEjAWOGrkwpbGu71Df0206zNBd9BLy963b3JeJ5P1huHwSo8M1/f
t8CiJzJrfXM11dvwtL1gHY6wuu4clJW9nFU32ESx/yT+S5LNmv1EPBaeql1ZmQIh
yVkfkCmt86a1qVEhv8IX7NgvI8KvwTqbmDezm/2ItjhoTjH0DtPQGy5cvgNbtgJr
jCalcxkB/oISRCWVuNgrtxAfsDdXZ3LPFxrnNuyw/n2z7PCBnwXK+lD/Sre6Vg6M
GqUMHSjqxmdoNNjfS2g5Gj8q8dE3gcRvs8HJABEBAAG0KXRlc3RpbmcgKHRlc3Rp
bmcpIDxpZGF3aWxscy5pd0BnbWFpbC5jb20+iQFRBBMBCAA7FiEEFgkbsX21BhFn
3yGyUtReyWV9a78FAmacKeICGwMFCwkIBwICIgIGFQoJCAsCBBYCAwECHgcCF4AA
CgkQUtReyWV9a7+FXgf/f2D/is/IzCKtrpb+U25/ivglrlIHxlorh13lKF71qB5l
s1oarsuP/WiDTfdxa+WUCnJBenM21rycYEvaYSmaCjRfg1USIQOOY1AP9zbGjiJz
2vUDoCamG09ynhlvucCUPsEtr5hy9jbugBaty1DbJFFn1UHR9W/0qXlxGrKruOqX
xVdz8ztWBIl3bl+hHkjhB0t80L8e80MEujXPIXyPEpCjpexPg9ZTWA8L37DiNpei
Tmg9nn5Td71J9nyccWFqJT2vVYYXILMxMHZ3pia3v3t6k2ojD6p1b+CAjGoomjjN
zfxyilyKia0HQc8kYDnYX2FCXQxef9KnV8EzH0Iu3rkBDQRmnCniAQgAtIpbR5Wb
8YmJnPMx/vhKsXtnmvpzjpk2fcdWWIMqay5p+QiT0m5ipIC2dLnDoJNDm/4Mm0+F
U+M5RNjROY6aZ4ZRGqpKtZSnrcnnPVrTiGpFcG5xnl1Si6iZxOQrZbDgSRSe9A53
sbNnKNv2uZ3Se/4skOn/TTlKZr2yS52hAWrKbRD0WzvHSJBpFyzOJ0vL5ida9WpN
LVCyEdKJpSjC2MaV/61r+zKl0cDmcmQsb0NDVKpQw8u3oG4R56fxUtDi9Usfi0uF
teRuKfdVvRV0ZYxenkFpUf5VJvbzJsM6oyvfZBjCeYlw67AE/Vk78ekMH/JkspgQ
2ilhGRYwsFMMMwARAQABiQE1BBgBCAAgFiEEFgkbsX21BhFn3yGyUtReyWV9a78F
AmacKeICGwwACgkQUtReyWV9a7/boQf4sv/rwuZchF3IEBl3ep+KDbrdy+PDPTM5
V42QHW5fYCcgyC6qVsvnJG1qFYeu2ldHwiYOsWHnx5guiC947vYDeDW9xmiMxeL4
loJZzo6BBVPMSUk5gYkMBjmoZsr8/nP3XL+T3qMXF/qcMQTDiMebxAHQme3bH7iX
y3+hfS9p/21kSgwiiSA2JO2CbeVv41GbqgZO6dmEUhumFChX6p3oS5tCHUuJ1yxl
GBfyqtMJWfNXJbVw4CuWERowBvbJuvqkqSw/LtwhFtazE3/sj+y8rf8uV0+lIgN5
4XM4Bh7/uuXhidM5jJuqqaTWGNoNl4clQsAyCFIYoHa6HKg2MWfa
=pl7c
-----END PGP PUBLIC KEY BLOCK-----
{% endhighlight %}