// Add event listener to theme toggle button
//Two button from .trigger and .menu-dropdown
try{
  document.querySelector("#theme-toggle").addEventListener("click", toggleTheme);
  document.querySelector("#theme-toggle-mobile").addEventListener("click", toggleTheme);
  const menu_dropdown = document.querySelector('.menu-dropdown');
} catch (e) {
  console.log(e)
}

// Function to toggle the theme
function toggleTheme() {  
 if (localStorage.getItem("theme") === "dark") {
   document.documentElement.classList.remove("dark-theme");
   document.documentElement.classList.add("light-theme");
   localStorage.setItem("theme", "light");
   loadDisqus();
 } else {
   document.documentElement.classList.remove("light-theme");
   document.documentElement.classList.add("dark-theme");
   localStorage.setItem("theme", "dark");
   loadDisqus();
 }  
}