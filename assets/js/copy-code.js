//This assumes that you're using Rouge; if not, update the selector
document.addEventListener("DOMContentLoaded", function(event) {
    
  const codeBlocks = document.querySelectorAll('.code-header + .highlight');
  const copyCodeButtons = document.querySelectorAll('.copy-code-button');
    
  copyCodeButtons.forEach((copyCodeButton, index) => {
    const code = codeBlocks[index].innerText;
    //iterates over each "Copy" button and adds an event listener
      copyCodeButton.addEventListener('click', () => { 

      // Copy the code to the user's clipboard
      window.navigator.clipboard.writeText(code);

      // Update the button text visually
      const { innerText: originalText } = copyCodeButton;
      copyCodeButton.innerText = 'Done';
      if (document.documentElement.classList.contains("dark-theme")) {
        copyCodeButton.style.backgroundColor = '#24487C'; 
      } else {
        copyCodeButton.style.backgroundColor = '#B3CFF7';
      }

      // After 2 seconds, reset the button to its initial UI
      setTimeout(() => {
        copyCodeButton.innerText = originalText;
        copyCodeButton.removeAttribute('style');
        copyCodeButton.style.color = null;
      }, 1000);
    });
  });
});