const checkbox_menu = document.querySelector("input[name=checkbox-menu]");
const menu_dropdown = document.querySelector('.menu-dropdown');

checkbox_menu.addEventListener('change', function() {

  if (this.checked) {
    menu_dropdown.classList.remove("menu-dropdown");
    menu_dropdown.classList.add("menu-dropdown-on");
    globalThis.scrollTo({top:0, left:0, behavior:"smooth"});

  } else {
    menu_dropdown.classList.remove("menu-dropdown-on");
    menu_dropdown.classList.add("menu-dropdown");
    globalThis.scrollTo({top:0, left:0, behavior:"smooth"});
    
  }
});
