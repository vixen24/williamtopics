---
layout: page
title: ChangeLog
permalink: /changelog
published: false
---

-----------------------------------------------------------------------------------------------------------
|     Date    |             Description            |                        Details                       |                   
-----------------------------------------------------------------------------------------------------------
| 08/30/2023  | - Launch website                   |                                                      |
|             | - Built pipeline for CI/CD         |                                                      |
|             | - Tweak website UI                 |                                                      |
|             | - Add dark theme                   |                                                      |
-----------------------------------------------------------------------------------------------------------
|03/06/2024   | Add Note feature and minor fixes   | Made minor changes to existing features and added a  |
|             |                                    | Note Page                                            |
-----------------------------------------------------------------------------------------------------------
|22/07/2024   | Add new post, note and revamp      | Revamped /about page, made it simple. Added pgp      |
|             | /about page                        | public key for confidential messaging                |
-----------------------------------------------------------------------------------------------------------
|26/07/2024   | Optimize dark theme                | Reorganized CSS to optimize theme transition, remove |
|             |                                    | blink between theme transitions                      |
-----------------------------------------------------------------------------------------------------------
|01/08/2024   | Redesign menu dropdown             | Redesign menu dropdown to improve mobile             |
|             |                                    | responsivenes                                        |
-----------------------------------------------------------------------------------------------------------
|12/08/2024   | Publish new post                   | Deploy from GitHub to Azure Web App                  |
|             |                                    |                                                      |
-----------------------------------------------------------------------------------------------------------
|12/08/2024   | Remove .html from posts URL        | Add permalink: pretty to _config.yml                 |
|             |                                    |                                                      |
-----------------------------------------------------------------------------------------------------------
|30/09/2024   | UI update                          | Design and add logo                                  |
|             |                                    | Change theme icon                                    |
|             |                                    | CHange font                                          |
-----------------------------------------------------------------------------------------------------------

To-Do
- Fade image caption - Done
- Add disqus comment - Done
- Add og property for site.image, see https://github.com/hlaueriksson/conductofcode.io, https://conductofcode.io/post/social-meta-tags-with-jekyll/
    - site.URL
    - site.image
    - others
- redesign inspo, https://www.lastweekinaws.com/blog/
- Review cloudfront invalidatecache code
    - Check if S3 prefix exist on cloudfront, if it does invalidate s3 bucket. 
- Delete S3 versions to create space on S3 bucket
- Renane CSS classes to random, https://github.com/JPeer264/node-rename-css-selectors, https://www.npmjs.com/package/rename-css-selectors